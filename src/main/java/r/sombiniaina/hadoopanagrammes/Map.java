/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package r.sombiniaina.hadoopanagrammes;

import java.io.IOException;
import java.util.Arrays;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 *
 * @author Lekazuz
 */
public class Map extends Mapper<Object, Text, Text, Text>{
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {  
        String keyout = value.toString();
        char[] chars = keyout.toCharArray();
        Arrays.sort(chars);
        keyout = new String(chars);
	context.write(new Text(keyout), value);
    }
}
