/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package r.sombiniaina.hadoopanagrammes;

import java.io.File;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

/**
 *
 * @author Lekazuz
 */
public class HadoopAnagrammes {

    public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
        Configuration conf = new Configuration();
        
        String[] ourArgs = new GenericOptionsParser(args).getRemainingArgs();
        
        if(ourArgs.length < 2){
            throw new IllegalArgumentException("Arguments must have a input path file and a output path directory");
        }
        HadoopAnagrammes.deleteDirectory(new File(ourArgs[1]));
        
        Job job = Job.getInstance(conf, "Annagramme 1.0.0");
        job.setJarByClass(HadoopAnagrammes.class);
        job.setMapperClass(Map.class);
        job.setReducerClass(Reduce.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        
        FileInputFormat.addInputPath(job, new Path(ourArgs[0]));
        FileOutputFormat.setOutputPath(job, new Path(ourArgs[1]));
        
        if(job.waitForCompletion(true))
            System.exit(0);
        System.exit(-1);
    }
    
    private static boolean deleteDirectory(File directory){
        if(directory.exists()){
            File[] allContents = directory.listFiles();
            if(allContents != null){
                for (File file : allContents) {
                    deleteDirectory(file);
                }
            }
            return directory.delete();
        }
        return true;
    }
}
