/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package r.sombiniaina.hadoopanagrammes;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

/**
 *
 * @author Lekazuz
 */
public class Reduce extends Reducer<Text, Text, Text, Text>{
    public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
	Iterator<Text> i = values.iterator();
	Set<String> anagrammesSet = new HashSet<>();
	while(i.hasNext())  {
            anagrammesSet.add(i.next().toString());
        }
        if(anagrammesSet.size() != 1){
            context.write(key, new Text(anagrammesSet.toString()));
        }
    }
}
