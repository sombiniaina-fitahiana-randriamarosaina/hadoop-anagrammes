### Objectif

On dispose d'une liste de mots courants de la langue anglaise (plus de 1000). On souhaite déterminer quels mots sont des anagrammes. On va tester le programme avec le fichier : **common_words_en_subset.txt** dans le projet.

### Définition

On rappelle qu'un mot est une anagramme d'un autre si leurs lettres sont identiques (par exemple, « lemon » et « melon »).

### Exécution
Ajouter 2 paramètres d'entrer : 
- common_words_en_subset.txt (le fichier d'entrer)
- results (le dossier de sortie)

### Enjoy guys
